# OpenML dataset: amazon_employee_access

https://www.openml.org/d/43900

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Amazon Employee Access (Kaggle Competition). The data consists of real historical data collected from 2010 & 2011.  Employees are manually allowed or denied access to resources over time. You must create an algorithm capable of learning from this historical data to predict approval/denial for an unseen set of employees.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43900) of an [OpenML dataset](https://www.openml.org/d/43900). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43900/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43900/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43900/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

